// Welcomes to the default provided MWJS (Miliways-JavaScript) script.
// The API refeence can be found at https://projects.sucs.org/milliways/mw/blob/master/mwjs.rst

// This script comes with a very verbose debugmode
// set it to a integer to enable a level of debugging
// setting it to 0 disables it.
// This can be set within the talker by issuing the command ",debug"
var debugmode = 0;
// As of https://projects.sucs.org/milliways/mw/commit/de6615925555f274348afa3a0ab22bf9a7229a2f
// We get more events in replay, this bool is used to track whether we have seen
// our orignal talker join event or not
var replayed = false;
// If we don't have any colour info then create space for it
if (mw.store["usercolours"] === undefined) {
	mw.store["usercolours"] = "{}";
}

// Dummy js dict/object for storing stuff locally
var usercolours = {}; //local var used ++speedy
usercolours = JSON.parse(mw.store["usercolours"]); //grab the info from mw.store ++slow
var commands = {};

// All readable colours using the old MW Colour codes
// MW now supports ANSI 256 colours codes - https://projects.sucs.org/milliways/mw/blob/master/colours.rst
var colourArray = ["kw","kr","ky","kg","kc","kb","km","kW","kR","kY","kG","kC","kB","kM","wr","wy","wg","wc","wb","wm","wK","wR","wY","wG","wC","wB","wM","ry","rg","rc","rb","rm","rK","rW","rY","rG","rC","rB","rM","yg","yc","yb","ym","yK","yW","yR","yG","yC","yB","yM","gc","gb","gm","gK","gW","gR","gY","gC","gB","gM","cb","cm","cK","cW","cR","cY","cG","cB","cM","bm","bK","bW","bR","bY","bG","bC","bM","mK","mW","mR","mY","mG","mC","mB","KW","KR","KY","KG","KC","KB","KM","WR","WY","WG","WC","WB","WM","RY","RG","RC","RB","RM","YG","YC","YB","YM","GC","GB","GM","CB","CM","BM"]

if (debugmode >= 1) {
	//print out info about the mw obj
	mw.print("DEBUG: mw object has: " + Object.getOwnPropertyNames(mw));

	//print out info about the mw.store stuff
	mw.print("DEBUG: mw.store object has: " + (JSON.stringify(mw.store)));

	//print out info about the usercolours
	mw.print("DEBUG: usercolours object has: " + JSON.stringify(usercolours));

}

// function to help register tlaker commands
function register(command, func, description) {
	var fname = "" + func.name;
	mw.command[command] = func;
	commands[command] = description;
}

// set debugmode level inside of mw
function setdebugmode(level) {
	var userinput = mw.input("Please enter a debugmode level: ");

	if (Number.isInteger(parseInt(userinput))) {
		debugmode = parseInt(userinput);
	} else {
		mw.print("Not a valid input! Must be an int.")
	}
}
register("debug",setdebugmode,"Set the debugmode level.");

// list all the colours, 1 per line
function colours() {
	for (var i = 0, len = colourArray.length; i < len; i++) {
		mw.print("\x1b" + colourArray[i] + " " + colourArray[i]);
	}
}
register("listallcolours", colours, "List all the colours!");

// function to pad a number so if it's single digits it reutrns as 2
// pad(4) == 04
// pad(18) == 18
function pad(n) {
	return ("0" + n).slice(-2);
}

// simple function to break down and return the useful to print parts of the
// unixtime we get from incomming events
function processUnixtime(unixtime) {
	var a = new Date(unixtime * 1000);
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = pad(a.getHours());
	var min = pad(a.getMinutes());
	var sec = pad(a.getSeconds());

	// return a timestamp in the format of <hh:mm:ss>
	return '<' + hour + ':' + min + ':' + sec + '>' + ' ';
}

// function to replace every instance of a word in a string
function replace(targetWord, newWord, text) {

	if (debugmode >= 2) {
		mw.print("DEBUG: replacing " + targetWord + " with " + newWord);
	}

	//split text based on spaces ' ' then regex on each item then implode
	var subTexts = text.split(" ");
	var newSubTexts = [];

	if (debugmode >= 2) {
		mw.print("DEBUG: text before replace: " + subTexts);
	}
	subTexts.forEach(function(element) {
		// the regex checks the start of a word is either new line (^)
		// or a word boundry (\b) whatever the fuck that means
		// checks for word boundry (\b) or line end ($) at the end
		// to stop parts of words being picked up when they shouldn't
		// "gi" just says match every (g) and ignore case (i)
		newSubTexts.push(element.replace(new RegExp("(^|\\b)"+targetWord+"($|\\b)","gi"),newWord));
	});
	if (debugmode >= 2) {
		mw.print("DEBUG: text after replace: " + newSubTexts);
	}
	text = newSubTexts.join(" ");
	return text;

	//attempt to do better but still had weird cases where it failed
	//return text.replace(new RegExp("(^|\b|\s)"+targetWord+"()","gi"), newWord);

	//the orignal function, ended up with substr matches
	//return text.replace(new RegExp(""+targetWord+"()","gi"), newWord);
}

// function to colour a word with a specifed colour using the LEGACY colours
function colourWord(word, colour) {
	return "\x1b" + colour + word + "\x1b--";
}

// generic function to apply custom predefined colouring to stuff
function applyColours(word) {

	//populate it with stuff we always want
	usercolours["imranh"] = "ck"; //imranh always a light cyan
	delete usercolours["undefined"];
	delete usercolours["null"];
	delete usercolours[""];

	//check if the mapping exists
	var result = usercolours.hasOwnProperty(word);

	//if there's already a static mapping then use that, otherwise randomise
	if (result) {
		return colourWord(word,usercolours[word]);
	} else {
		//bullshit a new colour and store it
		var colour = colourArray[Math.floor(Math.random()*colourArray.length)];
		usercolours[word] = colour;
		if (debugmode >= 1) {
			mw.print(JSON.stringify(usercolours));
		}
		mw.store["usercolours"] = JSON.stringify(usercolours); //update the mw.store object 
		return colourWord(word,colour);
	}

}

// simple fucntion to recolour users
function recolour() {

	var userinput = mw.input("Enter username, optionally followed by a space and a colour code: ");
	var inputs = userinput.split(" ");

	if (inputs[1]) {
		usercolours[inputs[0]] = inputs[1];
	} else {
		var colour = colourArray[Math.floor(Math.random()*colourArray.length)];
		usercolours[inputs[0]] = colour;
	}

	mw.store["usercolours"] = JSON.stringify(usercolours); //update the mw.store object

}
register("recolour",recolour,"Recolour a user. If you enter just a username, random colour is chosen if you enter a username followed by a space and then a colour that colour is used.");

// function to delete a user from the colours 'db'
function deleteColour() {
	var userinput = mw.input("Enter username: ");
	delete usercolours[userinput];
	mw.store["usercolours"] = JSON.stringify(usercolours); //update the mw.store object
}
register("deletecolour",deleteColour,"Delete a coloured username.");

// fuction to show all the colours assigned to users
function showcolours() {
	var cusers = [];
	for (var user in usercolours) {
		cusers.push(applyColours(user));
	}
	mw.print(cusers);
}
register("showcolours",showcolours,"Show all users and their colours.");

// our core event handler
function handleEvent(event) {
	if (debugmode >= 1) {
		mw.print(JSON.stringify(event));
	}

	// Loads of event types, see 'man mwjs'
	switch(event.type) {
		case "message_received":
			msg = event.data;

			if (debugmode >= 1) {
				mw.print("DEBUG: NEW MESSAGE of type: " + event.type);
				mw.print("DEBUG: msg.text: " + msg.text);
				mw.print("DEBUG: msg.unixtime: " + msg.unixtime);
				mw.print("DEBUG: msg.serial: " + msg.serial);
				mw.print("DEBUG: msg.ipc_type: " + msg.ipc_type);
				mw.print("DEBUG: msg.from_name: " + msg.from_name);
				mw.print("DEBUG: msg.to_name: " + msg.to_name);
				mw.print("DEBUG: msg.type: " + msg.type);
				mw.print("DEBUG: msg.excluded_name: " + msg.excluded_name);
				mw.print("DEBUG: msg.suffix: " + msg.suffix);
				if (msg.type == "say") {
					mw.print("DEBUG: msg.channel.id: " + msg.channel.id);
					mw.print("DEBUG: msg.channel.name: " + msg.channel.name);
					mw.print("DEBUG: msg.channel.topic: " + msg.channel.topic);
					mw.print("DEBUG: msg.channel.soundproof: " + msg.channel.soundproof);
				}
				mw.print("DEBUG: END OF MESSAGE");
			}

			var msgtextorignal = msg.text;

			// lets chnage how the time stamp is done
			var newTimestamp = processUnixtime(msg.unixtime);

			// highlight/colour people
			// update the values so we can use them in the future
			// as dreop in replacements
			msg.from_name = replace(msg.from_name,applyColours(msg.from_name),msg.from_name);
			// if excluded_name isn't null then fill that in
			if (msg.excluded_name != null) {
				msg.excluded_name = replace(msg.excluded_name,applyColours(msg.excluded_name),msg.excluded_name);
			}
			// if to_name isn't null then fill that in
			if (msg.to_name != null) {
				msg.to_name = replace(msg.to_name,applyColours(msg.to_name),msg.to_name);
			}

			// hightlight names in the main text
			Object.keys(usercolours).forEach(function(key){
				msg.text = replace(key, applyColours(key), msg.text);
			});

			//msg.text = replace("colour",colourWord("colour", "rk"),msg.text);

			// switch on different types of messages
			switch(msg.type) {
				case "say":
					// customise normal convo
					mw.print(newTimestamp + msg.from_name + ": " + msg.text);
					break;
				case "emote":
					// drop the : for emotes
					mw.print(newTimestamp + msg.from_name + " " + msg.text);
					break;
				case "raw":
					// raw detetction!
					// use the orignal text as some other
					// messages like topic setting come in
					// as raws and our colours mess it up
					mw.print(newTimestamp + "[" + msg.from_name + "]" + ": " + msgtextorignal);
					break;
				case "notsayto":
					// handle nosayto
					mw.print(newTimestamp + msg.from_name + ": " + "-(" + msg.excluded_name + ") " + msg.text);
					break;
				case "says":
				case "asks":
				case "whispers":
					// handle pm's/dm's
					mw.print(newTimestamp + msg.from_name + " " + colourWord(msg.type,"W-") +": " + msg.text);
					break;
				case "gag":
					// handle gsgs
					if (mw.message_to == mw.whoami) {
						//hide who gagged you
						mw.print(newTimestamp + ": " + msg.text);
						//don't hide who gagged you
						//mw.print(newTimestamp + "[" + msg.from_name + "]" + ": " + msg.text);
					} else {
						mw.print(newTimestamp + "[" + msg.from_name + "]" + ": " + msg.text);
					}
					break;
				case "ungag":
					// handle ungsgs
					// you get 2 ungag messages so we gots
					// to be geneirc,
					mw.print(newTimestamp + "[" + msg.from_name + "]" + ": " + msg.text);
					break;
				case "zod":
					// handle kicking from the talker
					if (mw.message_to == mw.whoami) {
						//hide who zod'd you
						mw.print(newTimestamp + ": " + msg.text);
						//don't hide who zod'd you
						//mw.print(newTimestamp + "[" + msg.from_name + "]" + ": " + msg.text);
					} else {
						mw.print(newTimestamp + "[" + msg.from_name + "]" + ": " + msg.text);
					}
					break;
				case "mrod":
					// handle kicking from the board
					if (mw.message_to == mw.whoami) {
						//hide who mrod'd you
						mw.print(newTimestamp + ": " + msg.text);
						//don't hide who mrod'd you
						//mw.print(newTimestamp + "[" + msg.from_name + "]" + ": " + msg.text);
					} else {
						mw.print(newTimestamp + "[" + msg.from_name + "]" + ": " + msg.text);
					}
					break;
				default:
					// leave the message alone if we don't
					// have rules for it
					mw.print(newTimestamp + "ERROR Unknown msg type: " + msg.type + ' ' + msg.from_name + ': ' + msg.text);
					break;
			}

			// return false to silence normal output
			return false;

			//stop handling message_recieved
			break;

		// handle talker join events
		case "talker_join":
			tje = event.data; //store the event data in tje

			if (debugmode >= 1) {
				mw.print("DEBUG: NEW MESSAGE of type: " + event.type);
				mw.print("DEBUG: tje.quiet: " + tje.quiet);
				mw.print("DEBUG: tje.user: " + tje.user);
				mw.print("DEBUG: tje.method: " + tje.method);
				mw.print("DEBUG: tje.channel: " + tje.channel);
			}

			// if it's us, then do a auto replay but not in
			// debugmode or if it's	a replayed event check by
			// comparing the time of event to current time
			// also set a flag for use later on
			if (tje.user == mw.whoami() && !debugmode >= 1 && tje.unixtime == Math.floor(Date.now()/1000)) {
				mw.exec("replay count 50");
				// set the replayed flag
				replayed = true;
			}

			//stop talker_join handling
			break;

		default:
			if (debugmode >= 1) {
				mw.print("Error! unhandeled event: " + JSON.stringify(event));
			}
			return true;
	}
}

// Push our eventhandler fuction to the list of funcs that handle events
mw.onevent.push(handleEvent);